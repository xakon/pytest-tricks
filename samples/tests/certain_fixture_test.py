import pytest

@pytest.fixture
def new_fixture():
    return 'foobar'

@pytest.fixture
def another_fixture(new_fixture):
    return 'foobar'

def test_abc(new_fixture):
    assert True

def test_def(another_fixture):
    assert True

def test_that_does_not_use_fixture():
    assert False
