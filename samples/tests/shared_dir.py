import shutil, tempdir


def pytest_configure(config):
    if is_master(config):
        config.shared_directory = tempdir.mktemp()


def pytest_unconfigure(config):
    if is_master(config):
        shutil.rmtree(config.shared_directory)


def pytest_configure_node(node):
    """xdist hook"""
    node.slaveinput['shared_dir'] = node.config.shared_directory


def is_master(config):
    """True if the code running the given pytest.config object is running in a xdist master
    node or not running xdist at all.
    """
    return not hasattr(config, 'slaveinput')


@pytest.fixture
def shared_directory(request):
    """Returns a unique and temporary directory which can be shared by
    master or worker nodes in xdist runs.
    """
    if is_master(request.config):
        return request.config.shared_directory
    return request.config.slaveinput['shared_dir']
