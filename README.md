py.test Tricks
==============

Executable samples from the excellent guide for py.test, [py.test Tricks][tricks]

[py.test][pytest] is a powerful testing framework for Python.
Unfortunately, with all this power comes a huge complexity and a
difficulty for me to learn it.  I found a nice [site][tricks],
where some tricks and advanced techniques of the framework are presented.
Still, I need a place to save the techniques, but also to be able to run
them and trace the results.


[pytest]:	http://pytest.org/
[tricks]:	http://hackebrot.github.io/pytest-tricks/
